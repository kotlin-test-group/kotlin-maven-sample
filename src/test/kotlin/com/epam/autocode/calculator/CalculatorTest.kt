package com.epam.autocode.calculator

import org.junit.jupiter.api.Test

class CalculatorTest {

    @Test
    fun testSum() {

        val calculator = Calculator()

        val sum = calculator.sum(1, 2)

        assert(sum == 3)
    }

    @Test
    fun testSubtraction() {

        val calculator = Calculator()

        val sum = calculator.subtract(1, 2)

        assert(sum == -1)
    }

    @Test
    fun testMultiply() {

        val calculator = Calculator("testMultiply")

        val product = calculator.multiply(-4, 2)

        assert(product == -8)
    }

    @Test
    fun testDivision() {

        val calculator = Calculator("testDivision")

        val quotient = calculator.divide(-12, -4)

        assert(quotient == 3)
    }
}